import sys

from PyQt5.QtWidgets import QApplication, QLCDNumber, QWidget
from PyQt5.QtCore import QTimer, QTime, Qt, QPoint
from PyQt5.QtGui import QIcon, QColor

import sys

class Clock(QLCDNumber):
    def __init__(self):
        super().__init__()

        title = "Clock"
        top = 400
        left = 400
        width = 450
        height = 300

        icon = "clock.png"

        self.setWindowTitle(title)
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setWindowFlags(Qt.WindowStaysOnTopHint)
        self.setGeometry(top, left, width, height)
        self.setWindowIcon(QIcon(icon))

        palette = self.palette()

        # foreground color
        #palette.setColor(palette.WindowText)

        self.setSegmentStyle(QLCDNumber.Filled)
        timer = QTimer(self)
        timer.timeout.connect(self.showTime)
        timer.start(1000)

        self.showTime()

    def showTime(self):
        time = QTime.currentTime()
        text = time.toString("hh:mm")
        if(time.second() % 2) == 0:
            text =text[:2] + '' + text[3]
        
        self.display(text)
    
    def mousePressEvent(self, event):
        self.oldPos = event.globalPos()

    def mouseMoveEvent(self, event):
        delta = QPoint (event.globalPos() - self.oldPos)
        #print(delta)
        self.move(self.x() + delta.x(), self.y() + delta.y())
        self.oldPos = event.globalPos()

app = QApplication(sys.argv)
clock = Clock()
clock.show()
app.exec()


# def timerEvent():
#     global time
#     time = time.addSecs(1)
#     print(time.toString("hh:mm:ss"))


# app = QtCore.QCoreApplication(sys.argv)

# timer = QtCore.QTimer()
# time = QtCore.QTime(0, 0, 0)

# timer.timeout.connect(timerEvent)
# timer.start(100)

# sys.exit(app.exec_())